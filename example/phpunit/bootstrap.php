<?php
/**
 * @file
 *   Bootsrap file for PHPUnit tests.
 */

require_once dirname(__FILE__) . '/../../sdb.interfaces.inc';
require_once dirname(__FILE__) . '/../../sdb.rel.1_to_n.inc';
require_once dirname(__FILE__) . '/../../sdb.rel.n_to_n.inc';
require_once dirname(__FILE__) . '/../../sdb.object.inc';
require_once dirname(__FILE__) . '/../../sdb.factory.inc';
require_once dirname(__FILE__) . '/../../example/sdb_example.db.tables.inc';

