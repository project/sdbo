<?php
/**
 * @file
 *   Main file of sdb examples.
 */

/**
 * Implements hook_menu().
 */
function sdbo_example_menu() {
  $items = array();

  $items['sdbo-example'] = array(
    'title' => 'SDBO Examples',
    'page callback' => 'sdbo_example_basic_page',
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['sdbo-example/basic'] = array(
    'title' => 'Basic SDB object',
    'access arguments' => array('access content'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
  );
  $items['sdbo-example/advanced'] = array(
    'title' => 'Advanced SDB object',
    'page callback' => 'sdbo_example_advanced_page',
    'access arguments' => array('access content'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  $items['sdbo-example/rel-1_to_n'] = array(
    'title' => 'REL Objects - 1_to_n',
    'page callback' => 'sdbo_example_rel_1_to_n_page',
    'access arguments' => array('access content'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );
  $items['sdbo-example/rel-n_to_n'] = array(
    'title' => 'REL Objects - n_to_n',
    'page callback' => 'sdbo_example_rel_n_to_n_page',
    'access arguments' => array('access content'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 4,
  );
  $items['sdbo-example/rel-advanced'] = array(
    'title' => 'REL Objects - advanced',
    'page callback' => 'sdbo_example_rel_advanced_page',
    'access arguments' => array('access content'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  return $items;
}

/**
 * Page callback displays and runs basic SDBObject examples.
 *
 * @return string
 */
function sdbo_example_basic_page() {

  $code = <<<EOF
// Schema definition
\$schema["sdbo_example_parents"] = array(
  "description" => "SDB Example parents table.",
  "fields" => array(
    "id" => array(
      "description" => "Object id",
      "type" => "serial",
      "not null" => TRUE,
      "unsigned" => TRUE),
    "uid" => array(
      "description" => "{user}.uid",
      "type" => "int",
      "not null" => TRUE,
      "default" => 0),
  ),
  "primary key" => array("id"),
);

// Implementation of SDBObject. Following code is just enough
// for basic CRUD operations.
class SDBExampleParent extends SDBObject {

  public \$uid;

  const TABLE = 'sdbo_example_parents';

  function getTable() {
    return self::TABLE;
  }
}
EOF;


  $output = _sdbo_example_dbg_msg($code, t('To work with objects mapped to database fields extend SDBObject class. To perform CRUD operations it is enough to implement getTable() method and define class attributes with same name as fields in the db table.<br />YOU DO NOT NEED TO DEFINE "id" ATTRIBUTE AS IT IS ALLREADY PROVIDED BY SDBObject AS PRIMARY KEY.'),
    t('Basic SDB object implementation'));

  $output .= _sdbo_example_dbg_msg('
  $object = new SDBExampleParent();
  $object->uid = "1";
  $object->save();', t('To create a new database entry an instance has to be created, its attributes initiated and save operation will perform the insert.'),
    t('Basic CRUD operations'));


  //
  $object = new SDBExampleParent();
  $object->uid = "1";
  $object->save();

  $output .= _sdbo_example_dbg_msg($object, t('The object after the save operation has its attribute "persisted" set to TRUE. The $this->isPersisted() method returns its value.'));

  $output .= _sdbo_example_dbg_msg('
  $object->uid = "4";
  $object->save();', t('Working with the same instance we set different value for "uid" attribute and call save method. It will perform db update.'));

  $object->uid = "4";
  $object->save();

  $output .= _sdbo_example_dbg_msg($object, t('You can check the "id" attribute value to make sure it is the same db entry.'));

  $object->delete();
  $output .= _sdbo_example_dbg_msg('$object->delete();', t('To delete the object from db call delete() method. Note that the instance is still alive until it runs out of scope. The only way to tell it is deleted is to check its persistency by getting result from isPersited().'));


  return $output;
}

/**
 * Page callback displays and runs advanced SDBObject examples.
 *
 * @return string
 */
function sdbo_example_advanced_page() {


  $code = <<<EOF
// More complex schema definition.
\$schema['sdbo_example_children'] = array(
  'description' => 'SDB Example children table.',
  'fields' => array(
    'id' => array(
      'description' => 'Object id',
      'type' => 'serial',
      'not null' => TRUE,
      'unsigned' => TRUE),
    'parent_id' => array(
      'description' => '{sdbo_example_parents}.id',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0),
    'created' => array(
      'description' => 'Time when object was created.',
      'type' => NULL,
      'mysql_type' => 'datetime',),
    'title' => array(
      'description' => 'Object title',
      'type' => 'varchar',
      'length' => 124,
      'not null' => TRUE,
      'default' => ''),
    'body' => array(
      'description' => 'Object body',
      'type' => 'text',
      'not null' => FALSE),
    ),
  'primary key' => array('id'),
);

// Implementation of SDBObject with unique key.
class SDBExampleChild extends SDBObject {

  // Persisted attributes
  public \$created;
  public \$title;
  public \$body;

  const TABLE = 'sdbo_example_children';

  function getTable() {
  //
    return self::TABLE;
  }

  /**
   * May return string or array depending on what fields you
   * need to use as keys.
   */
  function getUniqueKey() {
    return array('id', 'title');
  }

  /**
   * Due to the fact that getUniqueKey() returns array, getPrimaryKey() must be
   * implemented with return value of the actual primary key field name.
   */
  function getPrimaryKey() {
    return 'id';
  }

  /**
   * This method should implement logic of keys initialization.
   */
  function initUniqueKeyValues(\$vals) {
    if (isset(\$vals['id'])) {
      \$this->id = \$vals['id'];
    }

    if (isset(\$vals['title'])) {
      \$this->title = \$vals['title'];
    }
  }

  /**
   * Perform operations such as input sanitization.
   */
  function beforeSave() {
    if (is_string(\$this->created)) {
      \$this->created = strtotime(\$this->created);
    }
  }

  /**
   * In this case afterSave() calls afterLoad() to format
   * the created date value in case the value is load after save() call.
   */
  function afterSave() {
    \$this->afterLoad();
  }

  /**
   * May be used to format values retrieved from database.
   */
  function afterLoad() {
    if (is_int(\$this->created)) {
      \$this->created = date('d.m.Y H:i:s', \$this->created);
    }
  }
}
EOF;

  $output = _sdbo_example_dbg_msg($code, t('Following SDB implementation is ready to be used by SDBFactory. Please see the code below for individual methods description.<br /> Moreover for better data handling you can implement methods beforeSave(), afterSave(), beforeLoad(), afterLoad() and afterDelete().'),
    t('Extended SDB object implementation'));

  $data = array(
    'created' => date('d.m.Y H:i:s'),
    'title' => 'My testing title',
    'body' => 'My testing body'
  );

  $object = SDBFactory::initInstanceOf('SDBExampleChild', $data);
  $object->save();

  $output .= _sdbo_example_dbg_msg('
  $data = array(
    "created" => date("d.m.Y H:i:s"),
    "title" => "My testing title",
    "body" => "My testing body"
  );
  
  $object = SDBFactory::initInstanceOf("SDBExampleChild", $data);
  $object->save();', t('You can use SDBFactory to init the instance instead of assigning value to all of its attributes individualy. This is especially helpful when processing data submitted via forms.'),
    t('Initiating objects using SDBFactory'));

  $output .= _sdbo_example_dbg_msg($object, t('Object is persisted into db with all needed attributes.'));

  $loaded_object = SDBFactory::loadInstanceOf('SDBExampleChild', array('title' => $data['title']));

  $output .= _sdbo_example_dbg_msg('
  $loaded_object = SDBFactory::loadInstanceOf(\'SDBExampleChild\', array(\'title\' => $data[\'title\']));',
    t('Convenient way to load objects from database is to use SDBFactory::loadInstanceOf(). The second parameter is an associative array of key => values as defined by your getUniqueKey(). Also keep in mind that the logic of how the values of keys will be used is in initUniqueKeyValues() method.'),
    t('Loading objects from db using SDBFactory'));


  $persisted_objects = db_select(SDBExampleChild::TABLE, 't')->fields('t')->execute()->fetchAllAssoc('id');
  $sdb_objects = SDBFactory::initInstancesOf('SDBExampleChild', $persisted_objects);

  $output .= _sdbo_example_dbg_msg('
  $persisted_objects = db_select(SDBExampleChild::TABLE, "t")->fields("t")->execute()->fetchAllAssoc("id");
  $sdb_objects = SDBFactory::initInstancesOf("SDBExampleChild", $persisted_objects);
  ', t('To create a list of SDB objects you can use SDBFactory::initInstancesOf().'));

  $output .= _sdbo_example_dbg_msg($sdb_objects, t('Here is the array of SDB objects.'));

  foreach ($sdb_objects as $sdb_object) {
    $sdb_object->delete();
  }

  $output .= _sdbo_example_dbg_msg('
  foreach ($sdb_objects as $sdb_object) {
    $sdb_object->delete();
  }
  ', t('Now you can work with individual array items in the SDB conveninent way.'));

  return $output;

}

/**
 * Page callback displays and runs 1_to_n SDBObject examples.
 *
 * @return string
 */
function sdbo_example_rel_1_to_n_page() {
  
$code = <<<EOF
class SDBExampleParent extends SDBObject {

  public \$uid;

  const TABLE = 'sdbo_example_parents';

  function getTable() {
    return self::TABLE;
  }

  // This defines the relation to another class and its table.
  function getRelDefinition() {
    return array(
       'SDBExampleChildPolicyCascade' => array(
         'foreign_key' => 'parent_id',
         'del_policy' => 'cascade',
         'rel_type' => '1_to_n',
        ),
     );
  }

  function getPrimaryKey() {
    return 'id';
  }

  function getUniqueKey() {
    return array('id', 'uid');
  }

  function initUniqueKeyValues(\$init) {
    if (isset(\$init['id'])) {
      \$this->id = \$init['id'];
    }
    if (isset(\$init['uid'])) {
      \$this->uid = \$init['uid'];
    }
  }
}

// We just create an extended class so that we can reuse SDBExampleChild
// for all REL examples.
class SDBExampleChildPolicyCascade extends SDBExampleChild {

}
EOF;

  $output = _sdbo_example_dbg_msg($code, t('The difference from SDB advanced example is the getRelDefinition() method.'));

  $parent = new SDBExampleParent();
  $parent->uid = 1;
  $parent->save();

  
  // Add REL object
  $parent->rel('SDBExampleChildPolicyCascade')->add(array(
    'created' => date('d.m.Y H:i:s', time() - 24*60*60),
    'title' => 'Child object title 1',
    'body' => 'Child object body 1'
  ));
  $parent->rel('SDBExampleChildPolicyCascade')->add(array(
    'created' => date('d.m.Y H:i:s', time() - 24*60*60*2),
    'title' => 'Child object title 2',
    'body' => 'Child object body 2'
  ));
  $parent->rel('SDBExampleChildPolicyCascade')->add(array(
    'created' => date('d.m.Y H:i:s', time() - 24*60*60*3),
    'title' => 'Child object title 3',
    'body' => 'Child object body 3'
  ));

  $output .= _sdbo_example_dbg_msg('
  // We create SDBExampleParent object. Make sure the object is persisted
  // otherwise it will not be able to create child objects.
  $parent = new SDBExampleParent();
  $parent->uid = 1;
  $parent->save();

  // Add REL object
  $parent->rel("SDBExampleChildPolicyCascade")->add(array(
    "created" => date("d.m.Y H:i:s", time() - 24*60*60),
    "title" => "Child object title 1",
    "body" => "Child object body 1"
  ));
  $parent->rel("SDBExampleChildPolicyCascade")->add(array(
    "created" => date("d.m.Y H:i:s", time() - 24*60*60*2),
    "title" => "Child object title 2",
    "body" => "Child object body 2"
  ));
  $parent->rel("SDBExampleChildPolicyCascade")->add(array(
    "created" => date("d.m.Y H:i:s", time() - 24*60*60*3),
    "title" => "Child object title 3",
    "body" => "Child object body 3"
  ));
  ', t('To create a child object you need to call $parent->rel("SDBExampleChildPolicyCascade")->add() while passing data array into add() method.'),
    t('Creating child objects'));

  $child_rels = $parent->rel('SDBExampleChildPolicyCascade')->getAll(array(
    array('field' => 'created', 'direction' => 'DESC'),
    array('field' => 'title')
  ), 'id');

  $output .= _sdbo_example_dbg_msg('
  $child_rels = $parent->rel(\'SDBExampleChildPolicyCascade\')->getAll(array(
    array(\'field\' => \'created\', \'direction\' => \'DESC\'),
    array(\'field\' => \'title\')
  ), \'id\');
  ',
    t('Listing all child objects is also done through rel() interface. Additionaly you can pass in $order_by and $key_by params to control the output.'),
    t('Listing child objects')
  );

  $output .= _sdbo_example_dbg_msg($child_rels, t('Here is the result.'));

  // Delete all child objects.
  $parent->rel('SDBExampleChildPolicyCascade')->delAll();
  // Or delete parent that will cascade to child objects as well.
  $parent->delete();

  $output .= _sdbo_example_dbg_msg('
  // Delete all child objects.
  $parent->rel(\'SDBExampleChildPolicyCascade\')->delAll();
  // Or delete parent that will cascade to child objects as well.
  $parent->delete();
  ',
    t('To delete child objects you can either call delAll() on rel interface or in case you have set the delete policy to "cascade" the delete operation of the parent object will also delete all child objects.'),
    t('Deleting child objects')
  );

  return $output;
}

/**
 * Page callback displays and runs n_to_n SDBObject examples.
 *
 * @return string
 */
function sdbo_example_rel_n_to_n_page() {
  $code = <<<EOF

\$schema['sdbo_example_mapping'] = array(
  'description' => 'SDB Example mapping table.',
  'fields' => array(
    'parent_id' => array(
      'description' => 'Parent id',
      'type' => 'serial',
      'not null' => TRUE,
      'unsigned' => TRUE),
    'child_id' => array(
      'description' => 'Child id',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0),
  ),
  'primary key' => array('parent_id', 'child_id'),
);

class SDBExampleParent extends SDBObject {

  public \$uid;

  const TABLE = 'sdbo_example_parents';

  function getTable() {
    return self::TABLE;
  }

  // This defines the relation to another class and its table.
  function getRelDefinition() {
    return array(
       'SDBExampleNChildPolicyCascade' => array(
         'mapping_table' => 'sdbo_example_mapping',
         'my_key' => 'parent_id',
         'foreign_key' => 'child_id',
         'del_policy' => 'cascade', //keep|restrict|cascade
         'rel_type' => 'n_to_n',
        ),
     );
  }

  function getPrimaryKey() {
    return 'id';
  }

  function getUniqueKey() {
    return array('id', 'uid');
  }

  function initUniqueKeyValues(\$init) {
    if (isset(\$init['id'])) {
      \$this->id = \$init['id'];
    }
    if (isset(\$init['uid'])) {
      \$this->uid = \$init['uid'];
    }
  }
}

// We just create an extended class so that we can reuse SDBExampleChild
// for all REL examples.
class SDBExampleNChildPolicyCascade extends SDBExampleChild {

}
EOF;

  $output = _sdbo_example_dbg_msg($code, t('See the getRelDefinition() method for the difference from 1_to_n. Also mapping table must be provided.'));

  $output .= _sdbo_example_dbg_msg('
  $parent->rel(\'SDBExampleNChildPolicyCascade\')->getAll();
  $parent->rel(\'SDBExampleNChildPolicyCascade\')->delAll();
  ',
    t('All operations are performed the same way as in case of 1_to_n.'),
    t('Child objects operations')
  );

  return $output;
}

/**
 * Page callback displays and runs advanced SDBObject REL examples.
 *
 * @return string
 */
function sdbo_example_rel_advanced_page() {

  $parent = new SDBExampleParent();
  $parent->uid = 1;
  $parent->save();

  // Add REL object
  $parent->rel('SDBExampleChildPolicyCascade')->add(array(
    'created' => date('d.m.Y H:i:s', time() - 24*60*60),
    'title' => 'Child object title 1',
    'body' => 'Child object body 1'
  ));
  $parent->rel('SDBExampleChildPolicyCascade')->add(array(
    'created' => date('d.m.Y H:i:s', time() - 24*60*60*2),
    'title' => 'Child object title 2',
    'body' => 'Child object body 2'
  ));
  $parent->rel('SDBExampleChildPolicyCascade')->add(array(
    'created' => date('d.m.Y H:i:s', time() - 24*60*60*3),
    'title' => 'Child object title 3',
    'body' => 'Child object body 3'
  ));

  $output = _sdbo_example_dbg_msg('
  $parent = new SDBExampleParent();
  $parent->uid = 1;
  $parent->save();

  // Add REL object
  $parent->rel(\'SDBExampleChildPolicyCascade\')->add(array(
    \'created\' => date(\'d.m.Y H:i:s\', time() - 24*60*60),
    \'title\' => \'Child object title 1\',
    \'body\' => \'Child object body 1\'
  ));
  $parent->rel(\'SDBExampleChildPolicyCascade\')->add(array(
    \'created\' => date(\'d.m.Y H:i:s\', time() - 24*60*60*2),
    \'title\' => \'Child object title 2\',
    \'body\' => \'Child object body 2\'
  ));
  $parent->rel(\'SDBExampleChildPolicyCascade\')->add(array(
    \'created\' => date(\'d.m.Y H:i:s\', time() - 24*60*60*3),
    \'title\' => \'Child object title 3\',
    \'body\' => \'Child object body 3\'
  ));
  ', t('At first we create an object with several child objects.'));

  $rels = $parent->rel('SDBExampleChildPolicyCascade')->getAll(NULL, 'title');

  $output .= _sdbo_example_dbg_msg('
  // To access single child object you need to first call getAll() on rel
  // interface so that the internal static cache gets loaded.
  $rels = $parent->rel(\'SDBExampleChildPolicyCascade\')->getAll(NULL, \'title\');

  // Will output the same as the line below
  var_dump($parent->rel(\'SDBExampleChildPolicyCascade\')->getByObjectKey(\'Child object title 2\'));
  var_dump($rels[\'Child object title 2\']);
  ',
    t('There are two ways to access a single child object. The example below shows how you can access child objects by other db field than primary key.'),
    t('Accessing single child object')
  );

  $output .= _sdbo_example_dbg_msg($parent->rel('SDBExampleChildPolicyCascade')->getByObjectKey('Child object title 2'),
    'Output of $parent->rel(\'SDBExampleChildPolicyCascade\')->getByObjectKey(\'Child object title 2\')');
  $output .= _sdbo_example_dbg_msg($rels['Child object title 2'], 'Output of $rels[\'Child object title 2\']');

  $res = db_select(SDBExampleChildPolicyCascade::TABLE, 't')
      ->fields('t')->condition('t.parent_id', $parent->id)
      ->execute()->fetchAllAssoc('id');

  $parent2 = SDBFactory::loadInstanceOf('SDBExampleParent', array('id' => $parent->id), TRUE);
  $parent2->rel('SDBExampleChildPolicyCascade')->initObjects($res);

  $output .= _sdbo_example_dbg_msg('
  $parent2 = SDBFactory::loadInstanceOf(\'SDBExampleParent\', array(\'id\' => $parent->id), TRUE);
  
  $res = db_select(SDBExampleChildPolicyCascade::TABLE, \'t\')
      ->fields(\'t\')->condition(\'t.parent_id\', $parent->id)
      ->execute()->fetchAllAssoc(\'id\');
      
  $parent2->rel(\'SDBExampleChildPolicyCascade\')->initObjects($res);
  ', t('The following code will create a new instance of SDBExampleParent. The TRUE parameter passed into loadInstacneOf() will secure that the initialization logic will hit database - that is making sure we do not work with cached object.<br />' .
      '$res variable will be loaded by data from database and passed into initObjects() method of rel interface. This will initiate child objects of SDBExampleParent. The initObjects() method ' .
      'is supposed to be used when initializing lists of SDBObjects together with theirs REL objects from single data source.'),
    t('Instantiating REL objects from external data source'));
  $output .= _sdbo_example_dbg_msg($parent2->rel('SDBExampleChildPolicyCascade')->getAll());

  return $output;
}

/**
 * Debug output formatter.
 *
 * @param $vars
 * @param string $msg
 * @param string $title
 * @return string
 */
function _sdbo_example_dbg_msg($vars, $msg = '', $title = '') {
  if (is_string($vars)) {
    $dump = "CODE:\n" . $vars;
  }
  else {
    ob_start();
    var_dump($vars);
    $dump = "DUMP:\n" . ob_get_clean();
  }

  $output = "<h2>$title</h2>";

  if ($msg) {
    $output .= $msg;
  }

  $output .= '<div style="font-size: 11px; max-height: 300px; overflow: auto; background-color: white; border: 1px solid silver;"><pre style="color:#000;">'.$dump.'</pre></div>';

  return $output;
}
